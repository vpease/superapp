import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mergeMap, catchError } from 'rxjs/internal/operators';
import * as rssParser from 'react-native-rss-parser';
import { throwError, Observable } from 'rxjs';

import { Rss } from '../model/rss';

@Injectable({
  providedIn: 'root'
})
export class ServiceFeedService {
  constructor(private http: HttpClient) {
    }

    public get(url: string) {
      return this.http.get(url, { 'responseType': 'text'})
      .pipe(
        mergeMap((data) => {
          return rssParser.parse(data);
        }),
        catchError(this.handleError('getRSS'))
      ) as Observable<Rss>;
    }
  public load(url: string) {
    return new Promise((resolve, reject) => {
      fetch(url)
      .then((resp) => resp.text())
      .then((respData) => rssParser.parse(respData))
      .then((rss) => {
        resolve(rss);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
  private handleError(operation: string = 'operation') {
    return (error: any) => {
      error.operation = operation;
      return throwError(error);
    };
  }
}
