import { TestBed } from '@angular/core/testing';

import { ServiceFeedService } from './service-feed.service';

describe('ServiceFeedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceFeedService = TestBed.get(ServiceFeedService);
    expect(service).toBeTruthy();
  });
});
