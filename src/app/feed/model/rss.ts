export class Rss {
    public type: string;            // either `rss-v2` or `atom-v1`
    public title: string;           // title of the channel
    public links: [{
          url: string,           // url of the channel
          rel: string            // type of url (eg. alternate)
        }];
    public description: string;     // description of the channel
    public language: string;        // language of the channel in `en-us`
    public copyright: string;       // copyright information about the channel
    public authors: [{
          name: string           // channel author names
        }];
    public lastUpdated: string;     // last updated date for the channel
    public lastPublished: string;   // last published date for the channel
    public categories: [{
          name: string           // categories the channel belong too
        }];
    public image: {
          url: string;           // channel image url
          title: string;         // channel image title
          description: string;   // channel image description
          width: string;         // channel image width (pixels)
          height: string         // channel image height (pixels)
        };
    public itunes: {                   // itunes specific channel information
          author: [{
            name: string         // channel author names
          }],
          block: string,         // if `yes` then the entire podcast isn't shown in iTunes directory
          categories: [{
            name: string,        // channel category names
            subCategories: [{
              name: string       // sub category names
            }]
          }],
          image: string,         // channel image url
          explicit: string,      // `yes`/`no` to indicate if channel contains explicit content
          complete: string,      // `yes` indicates the feed won't publish any new items in the future
          newFeedUrl: string,    // a url pointing to the new feed location
          owner: {
            name: string,        // owner name of the channel
            email: string,       // owner email address of the channel
          },
          subtitle: string,      // sub title of the channel
          summary: string,       // summary of the channel
        };
    public items: [{                   // list of items in the feed
          id: string,            // item id
          title: string,         // item title
          imageUrl: string,      // item image url
          links: [{
            url: string,         // item link url
            rel: string          // type of item link
          }],
          description: string,   // item description
          content: string,       // item HTML content
          categories: [{
            name: string         // categories the item belongs too
          }],
          authors: [{
            name: string         // item author names
          }],
          published: string,     // item published date
          enclosures: [{
            url: string,         // item media url
            length: string,      // item media length (bytes)
            mimeType: string     // item media mime type (eg audio/mpeg)
          }],
          itunes: {                 // itunes specific item information
            authors: [{
              name: string,      // item author names
            }],
            block: string,       // `yes` indicates the item won't be displayed in the iTunes directory
            duration: string,    // HH:MM:SS length of the item
            explicit: string,    // `yes`/`no` to indicate if item contains explicit content
            image: string,       // image url for the item
            isClosedCaptioned: string, // `yes` indicates if the item contains closed captioning
            order: string,       // item order number
            subtitle: string,    // item subtitle
            summary: string,     // item summary
          }
        }];
    }
